Terdapat tiga user yang berbeda di dalam sistem perkebunan yaitu Pimpinan, Admin dan Pegawai Kebun. Pimpinan memiliki level kuasa tertinggi di sistem perkebunan ini untuk mengatur otoritas, pimpinan juga bisa mengakses semua fitur tanpa terkecuali. Kemudian pimpinan juga memiliki akses dimenu persetujuan dimana pimpinan menyetujui permintaan yang diminta oleh admin. 
Pimpinan dapat mengakses seluruh menu yang ada didalam sistem. Menu yang dapat diakses oleh pimpinan antara lain: 
1.	Aktifitas, dalam menu aktivitas Pimpinan hanya melihat laporan cuaca, hama, dan pupuk.
2.	Kelola Keuangan, pada menu ini pimpinan melakukan pencatatan pemasukan dan pengeluaran kebun.
3.	User, pimpinan dapat menambahkan user dan juga membatasi hak akses setiap user dalam mengakses sistem. Pimpinan juga mengatur semua otoritas yang ada di sistem tersebut.
4.	Inventory, mencatat barang apa saja yang dimiliki oleh perusahaan contohnya alat perkebunan berupa selang, cangkul dan lain-lain, lalu pimpinan dapat melihat barang apa saja yang sudah dikembalikan oleh pegawai kebun.
5.	Persetujuan, pada menu ini pimpinan menyetujui permintaan yang diminta oleh admin
6.	Hasil Panen, pada menu ini pimpinan mencatat hasil panen yang diperoleh disetiap kebun yang terdata dalam sistem ini, setiap kegiatan panen pimpinan memasukkan data update hasil panen setiap kebun.
7.	Laporan, menu laporan digunakan pimpinan dalam melihat laporan administrasi keuangan berupa pemasukan dan pengeluaran yang terdata di dalam sistem.
8.	Setup, pada menu setup pimpinan dapat memasukkan data lahan baru atau data kebun baru ketika perusahaan ingin menambah lahan kebun baru, pimpinan juga dapat memasukkan data harga hasil kebun per setiap kebun yang tercatat oleh sistem.
9.	Profil, Pimpinan dapat mengubah data diri dan mengubah password untuk masuk kedalam sistem.
Admin dapat mengakses semua fitur kecuali persetujuan dan otoritas, berikut menu yang dapat diakses oleh admin antara lain:
1.	Aktifitas, admin cuman bisa melihat kegiatan yang dilakukan oleh pegawai di kebun seperti melihat pencatatan hama, cuaca dan pupuk.
2.	Kelola Keuangan, pada menu ini admin dapat melakukan CRUD atau Create, Read, Update, Delete untuk melakukan pencatatan pemasukan dan pengeluaran dari hasil panen.
3.	User, pada menu ini admin melakukan penambahan jika ada pekerja baru
4.	Inventory, pada menu ini admin melakukan permintaan barang yang akan digunakan oleh pegawai kebun kemudian pimpinan melakukan persetujuan, kemudian melakukan penambahan aset seperti cangcul, selang air dan lain-lain, selanjutnya admin melakukan pengajuan ke pimpinan bahwa ada pegawai kebun yang mengunakan aset kebun, jika pegawai sudah menggunakan aset tersebut admin akan melakukan pengajuan pengembalian barang yang nanti nya akan dilihat oleh pimpinan.
5.	Hasil Panen, pada menu ini admin melakukan update terkait hasil panen yang nanti nya akan dilihat oleh pimpinan.
6.	Laporan, menu laporan digunakan pimpinan dalam melihat laporan administrasi keuangan berupa pemasukan dan pengeluaran yang terdata di dalam sistem.
7.	Setup, pada menu ini admin dapat melakukan penambahan kebun baru dan juga blok  dari kebun tersebut, kemudian admin juga yang akan melakukan update terkait harga dari penjualan kebun tersebut.
8.	Profil, pada menu ini admin dapat mengubah data diri dan password untuk masuk kedalam sistem.
Pegawai Kebun memiliki hak akses berupa:
1.	Aktifitas, dimana pegawai kebun mencatat keadaan yang tejadi di lapangan seperti pencatatan keadaan cuaca, pencatatan hama, dan juga pencatatan pupuk dengan memasukkan data tersebut ke sistem.
2.	Inventory, pada menu ini pegawai cuman diberi akses fitur pemakaian, dimana pegawai mencatat barang apa saja yang dipinjam oleh pegawai untuk dikembalikan kembali.
3.	Profil, pegawai dapat megubah data diri dan mengubah password untuk masuk kedalam sistem.
 
